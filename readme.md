#NpTextJuggle

##About
A small pure javascript library for letting text fly in from any random place to the original place and return to those random places if you want to.
This can be triggered several times or just once per session (stored in sessionStorage)

##Usage
Create an element with an id. Then add the next code:

    var juggle1 = new NpTextJuggle('your-id', options);

And thats it. Now you can trigger it by calling `run`:

    juggle1.run();

##Documentation

###Instalation

Download the code from the [repository][bitbucket]

Install by using bower:

    bower install --save https://bitbucket.org/Navelpluisje/nptextjuggle.git

###Option

* **id**: The id of the DOM-element. Works only on Id's
* **options**: Set of default options. This is an object with the following attributes:
    - *duration*: The total duration of the juggle without the last transition duration in milliseconds
    - *transDuration*: The length of a transition per item
    - *doReturn*: Only implode or also exploding when finished
    - *delay*: The delay before activating the juggle in milliseconds

###Methods

* **run()**: Start the juggle
* **get(*key*)**: Get the value of the given option key
* **set(*key, value*)**: Set the value of an option
* **getTotalDuration()**: Get the total duration of the juggle in milliseconds

[bitbucket]: https://bitbucket.org/Navelpluisje