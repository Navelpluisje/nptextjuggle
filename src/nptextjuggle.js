/**
 * Copyright (c) 2016, Erwin Goossen
Permission to use, copy, modify, and/or distribute this software for any purpose 
with or without fee is hereby granted, provided that the above copyright notice 
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS 
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
THIS SOFTWARE.
 */


/**
 * Create a textJuggle element. All the characters in a string will be placed with a random offset. 
 * While triggering they will appear from different directions.
 * 
 * @return {Object}       Object with all the public accessable functions and params 
 */
var NpTextJuggle = function (element, opts) {
	var self = this;

	self.text;
	self.theText;
	self.duration;
	self.steps;
	self.stepDelay;
	self.transDuration = 1000;
	self.runnable = true;

	function set(key, value) {
		if (key[0] !== '_') {
			self[key] = value;
			_setInitials();
		}
	}
	
	function get(key) {
		if (key[0] !== '_') {
			return self[key];
		}
	}

	/**
	 * Randomly generate a position
	 * @private
	 * @return {Number} The generated number which can be both positive and negative
	 */
	function _getPosition() {
		return Math.sign(.5 - Math.random()) * (2000 *  Math.random());
	}

	/**
	 * Reset all the spans to their original position
	 *
	 * @param {Object} obj Dom object to handle 
	 */
	function _setOriginals() {
		var elements = self.text.querySelectorAll('span'),
		i = 0;

		for (i; i < elements.length; i += 1) {
			elements[i].style.transform = 'none';
			elements[i].style.opacity = 1;
		}
		self.text.style.opacity = 1;
	}


	/**
	 * Set the default values or overwrite them with the options
	 *
	 * @param {Object} options set of options
	 * @param {String} options.elementId The id of the object to juggle
	 * @param {Number} options.duration  The total duration of the juggle without transition duration
	 * @param {Number} options.transDuration The length of a transition
	 * @param {Boolean} options.doReturn Only implode or also exploding when finished
	 * @param {Number} options.delay The delay before activating the juggle in milliseconds
	 */
	function _setVariables(options) {
		self.text = document.getElementById(element);
		self.elementId = element;
		self.duration = options.duration || 2000;
		self.transDuration = options.transitionLength || 1000; 
		self.doReturn = options.doReturn || false;
		self.delay = options.delay || 5;
		self.once = options.once || false; 
		if (self.once && _checkSession()) {
			self.runnable = false;
		}
	}

	/**
	 * Wrap every character of the text into a span element
	 *
	 * @return {[type]} [description]
	 */
	function _spannifyText() {
		var i = 0,
			res = '';

		/**
		 * Needs to be done by creating a string-content because appendCild does not
		 * support adding spans into a heading tag.
		 */
		for (i; i < self.steps; i += 1) {
			res += '<span>' + self.theText[i] + '</span>';
		}

		self.text.innerHTML = res;
	}

	function _setStepLength() {
		self.theText = self.text.textContent;
		self.steps = self.theText.length;
		self.stepDelay = self.duration / self.steps;
	}

	/**
	 * Get the session value
	 *
	 * @return {Number} The value
	 */
	function _checkSession() {
		return +sessionStorage.getItem('nptj-' + self.elementId);
	}

	/**
	 * Set the id to the sessionStorage
	 */
	function _setSession() {
		sessionStorage.setItem('nptj-' + self.elementId, 1);
	}

	/**
	 * First we get all the spans.
	 * Create the seperate offsets, delay and opacity for the created spans.
	 */
	function _setOffsets() {
		var elements = self.text.querySelectorAll('span'),
			i = 0;

		for (i; i < elements.length; i += 1) {
			elements[i].style.transform = 'translateY(' + _getPosition() + 'px) translateX(' + _getPosition() + 'px)';
			elements[i].style.transitionDelay = self.stepDelay * i / 1000 + 's';
			elements[i].style.transitionDuration = self.transDuration + 'ms';
			elements[i].style.opacity = 0;
		}
	}

	/**
	 * Calling a group of functions for (re)setting the values.
	 * Can also be called afetr changing values and reset.
	 */
	function _setInitials() {
		_setStepLength();
		_spannifyText();
		_setOffsets();		
	}


	function _runJuggle() {
		_setOriginals();
		if (self.doReturn) {
			setTimeout(_setOffsets, self.duration + self.stepDelay + self.transDuration);		
		}		
	}

	function _init(opts) {
		_setVariables(opts);
		if (self.runnable) {
			_setInitials();		
		} else {
			self.text.style.opacity = 1;
		}
	}

	/**
	 * Start running the juggle. Set the delay if we have one
	 * Check if we're runnable and set the id to the sessionStorage
	 */
	function run() {
		if (self.runnable) {\
			_setSession();
			setTimeout(_runJuggle, self.delay);
		}
	}

	/**
	 * Get the total duration of the juggle
	 *
	 * @return {Number} Duration of the juggle in milliseconds
	 */
	function getTotalDuration() {
		if (!self.runnable) {
			return 0;
		}

		var total = self.duration + self.transDuration;
		if (self.doReturn) {
			total *= 2;
		}
		return total;
	}

	_init(opts);

	/**
	 * Return all our publuc accessible stuff over here
	 */
	return {
		run: run,
		set: set,
		get: get,
		getTotalDuration: getTotalDuration
	}
};
