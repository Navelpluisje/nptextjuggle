var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

gulp.task('scripts', function () {
    return gulp.src( 'src/nptextjuggle.js' )
        .pipe( uglify() )
        .pipe( rename( 'nptextjuggle.min.js' ) )
        .pipe( gulp.dest( 'build' ) )
});